# Backup container image

Requires the following environment variables to be set:

| Name                     | Description                                            |
| ------------------------ | ------------------------------------------------------ |
| `S3_ENDPOINT_URL`        | Endpoint for the S3 provider                           |
| `S3_REGION`              | S3 region                                              |
| `S3_BUCKET`              | Bucket name                                            |
| `S3_ACCESS_KEY`          | Access key used for authentication                     |
| `S3_SECRET_KEY`          | Secret key used for authentication                     |
| `MONGO_HOST`             | MongoDB instance host                                  |
| `MONGO_PORT`             | MongoDB instance port                                  |
| `MONGO_ROOT_USER`        | MongoDB root user                                      |
| `MONGO_ROOT_PASSWORD`    | MongoDB root password                                  |
| `MONGO_CERT_CA_FILE`     | Path to the CA file for SSL connection                 |
| `MONGO_CERT_CLIENT_FILE` | Path to the client certificate file for SSL connection |
