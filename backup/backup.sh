#!/bin/bash -xe

set -Eeuox pipefail

DATE=$(date +'%Y%m%d%H%M')

mkdir -p /tmp/backup/

mongodump -h $MONGO_HOST \
  --port $MONGO_PORT \
  -u $MONGO_ROOT_USER \
  -p $MONGO_ROOT_PASSWORD \
  --ssl \
  --sslCAFile=$MONGO_CERT_CA_FILE \
  --sslPEMKeyFile=$MONGO_CERT_CLIENT_FILE \
  --tlsInsecure \
  --gzip \
  --archive=/tmp/backup/$DATE.archive.gz
gzip -t /tmp/backup/$DATE.archive.gz

aws configure set plugins.endpoint awscli_plugin_endpoint

cat > ~/.aws/config <<EOF
[plugins]
endpoint = awscli_plugin_endpoint

[default]
region = $S3_REGION
s3 =
  endpoint_url = $S3_ENDPOINT_URL
  signature_version = s3v4
  max_concurrent_requests = 100
  max_queue_size = 1000
  multipart_threshold = 50MB
  multipart_chunksize = 10MB
s3api =
  endpoint_url = $S3_ENDPOINT_URL
EOF

cat > ~/.aws/credentials <<EOF
[default]
aws_access_key_id=$S3_ACCESS_KEY
aws_secret_access_key=$S3_SECRET_KEY
EOF

aws s3 cp /tmp/backup/$DATE.archive.gz s3://$S3_BUCKET
