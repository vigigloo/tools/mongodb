resource "helm_release" "mongodb" {
  chart           = "mongodb"
  repository      = "https://charts.bitnami.com/bitnami"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  set {
    name  = "architecture"
    value = var.architecture
  }
  dynamic "set" {
    for_each = var.auth_enabled == null ? [] : [var.auth_enabled]
    content {
      name  = "auth.enabled"
      value = var.auth_enabled
    }
  }
  dynamic "set" {
    for_each = var.auth_rootUser == null ? [] : [var.auth_rootUser]
    content {
      name  = "auth.rootUser"
      value = var.auth_rootUser
    }
  }
  dynamic "set_sensitive" {
    for_each = var.auth_rootPassword == null ? [] : [var.auth_rootPassword]
    content {
      name  = "auth.rootPassword"
      value = var.auth_rootPassword
    }
  }
  dynamic "set" {
    for_each = var.auth_usernames == null ? [] : var.auth_usernames
    content {
      name  = "auth.usernames[${set.key}]"
      value = set.value
    }
  }
  dynamic "set_sensitive" {
    for_each = var.auth_passwords == null ? [] : var.auth_passwords
    content {
      name  = "auth.passwords[${set_sensitive.key}]"
      value = set_sensitive.value
    }
  }
  dynamic "set" {
    for_each = var.auth_databases == null ? [] : var.auth_databases
    content {
      name  = "auth.databases[${set.key}]"
      value = set.value
    }
  }
  dynamic "set" {
    for_each = var.auth_replicaSetKey == null ? [] : [var.auth_replicaSetKey]
    content {
      name  = "auth.replicaSetKey"
      value = var.auth_replicaSetKey
    }
  }
  dynamic "set" {
    for_each = var.auth_existingSecret == null ? [] : [var.auth_existingSecret]
    content {
      name  = "auth.existingSecret"
      value = var.auth_existingSecret
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }
}
